package com.optimum.problem8;

import java.util.Scanner;

public class Problem8 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();
		
		int i,k;
		
		for ( i = userInput; i > 0; i --) { // i begins from the value of user input and decrement by 1 in each iteration till i is more than 0
			k = i - 2;
			for (int j = 0; j < i; j++) { // j will print till less that i
				System.out.print("*");				
			}
			for (int m = 0; m < k; m++) { // additional '*' to be printed. condition for number of '*' is value of current user input - 2 
				System.out.print("*");								
			}
			if(i >2) {
				System.out.println("\n");
			} else {
				System.out.println();
			}
		}		
	} // end of main()
} // end of Problem8 Class

//	Output:
//	Enter a number: 5
//	********
//	
//	******
//	
//	****
//	
//	**
//	*