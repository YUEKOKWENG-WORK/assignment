package com.optimum.problem16;

// Generate Fibonacci Series using For loop
// The Fibonacci sequence: 0, 1, 1, 2, 3, 5, 8, 13, 21

public class Problem16A {

	public static void main(String[] args) {
		
		int num1 = 0, num2 = 1, sum = 0;
		// print the first two number of the Fibonacci Series
		System.out.print(num1 + ", " + num2);
		
		// condition the number of Fibonacci number to be printed
		int numberOfFibonacci = 9;
		
		// start for loop on the third Fibonacci number
		// assign the sum of num1 and num2 to integer variable sum
		// print out the sum
		// assign the value of num2 to the num1
		// assign the sum to num2 (i.e sum is the sum of second number and the sum of the previous two numbers
		for (int i = 2; i < numberOfFibonacci; i++) {
			sum = num1 + num2;
			System.out.print(", " + sum);
			num1 = num2;
			num2 = sum;
		}
	} // end of main()

} // end of Problem16A

//	Output:
//	0 1 1 2 3 5 8 13 21
