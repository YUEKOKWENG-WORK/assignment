package com.optimum.problem16;

//Generate Fibonacci Series up to a given number
//The Fibonacci sequence: 0, 1, 1, 2, 3, 5, 8, 13, 21

public class Problem16C {

	public static void main(String[] args) {

		// given number is declare as up to 21
		int numberOfFibonacci = 21, num1 = 0, num2 = 1, sum = 0;
		
		// while loop condition runs till less than or equal to number declared as the fibonacci number
		System.out.print("Fibonacci Series up to " + numberOfFibonacci + ": ");
		while(num1 <= numberOfFibonacci) {
			System.out.print(num1 != numberOfFibonacci ? num1 + ", " : num1);
			sum = num1 + num2;
			num1 = num2;
			num2 = sum;
			
		}// end of while loop
	} // end of main()
} // end of Problem16C Class
