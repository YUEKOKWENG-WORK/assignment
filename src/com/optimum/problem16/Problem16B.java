package com.optimum.problem16;

//Generate Fibonacci Series using While loop
//The Fibonacci sequence: 0, 1, 1, 2, 3, 5, 8, 13, 21

public class Problem16B {

	public static void main(String[] args) {

		int num1 = 0, num2 = 1, sum = 0, counter = 1, numberOfFibonacci = 9;
		
		// while condition to run from 1 till equal to number of Fibonacci values
		// set num1 as 0 and num2 to 1
		// print out value of num1 in each iteration
		while(counter <= numberOfFibonacci) {
			System.out.print(counter != numberOfFibonacci ? num1 + ", " : num1);
			sum = num1 + num2;
			num1 = num2;
			num2 = sum;
			counter++;
			
		}// end of while loop
	} // end of main()
} // end of Problem18
