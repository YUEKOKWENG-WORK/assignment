package com.optimum.problem10;

public class Problem10 {

	public static void main(String[] args) {
		// declare and initialize 3 arrays of different size and value
		int [] input1 = {1, 5, 10, 20, 40, 80};
		int [] input2 = {6, 7, 20, 80, 100}; 
		int [] input3 = {3, 4, 15, 20, 30, 70, 80, 120}; 
		
		// create a object reference to FindCommonValue class
		FindCommonValue refCommonValue = new FindCommonValue();
		System.out.print("Common elements are: ");
		// call the method FindCommonNumber() with the three arrays passed in as arguments
		refCommonValue.FindCommonNumber(input1, input2, input3);
		
		

	} // end of main()
} // end of Problem10 class

//	input1 = {1, 5, 10, 20, 40, 80} 
//	input2 = {6, 7, 20, 80, 100} 
//	input3 = {3, 4, 15, 20, 30, 70, 80, 120} 
//	Output: 20, 80