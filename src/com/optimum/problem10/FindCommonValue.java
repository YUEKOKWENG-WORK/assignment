package com.optimum.problem10;

public class FindCommonValue {

	void FindCommonNumber(int [] input1, int [] input2, int []input3){
		
		int i = 0,j = 0,k = 0;
		
		// while loop condition to check the common value till the end of any of the three an array
		while(i < input1.length && j < input2.length && k < input3.length) {
			// if current array 1 index value equal to current array 2 index value
			// and if current array 2 index value equal to current array 3 index value
			// print the current array 1 index value and increment all 3 arrays to next index
			if(input1[i] == input2[j] && input2[j] == input3[k]) {
				System.out.print(input1[i] + " ");
				i++;j++;k++;
			}
			// if the current array 1 index value is less than current array 2 index value
			// increase array 1 index
			else if(input1[i] < input2[j]){
				i++;
			}
			// if the current array 2 index value is less than array 3 index value
			// increase the array 2 index
			else if(input2[j] < input3[k]){
				j++;
			}
			else {
			// if the current array 3 index value is the smallest, increase the index to the next value
				k++;
			}
		} // end of while loop
	} // end of FindCommonNumber method
} // end of FindCommonValue Class
