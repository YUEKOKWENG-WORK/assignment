package com.optimum.problem13;

import java.util.Scanner;

public class Problem13 {

	public static void main(String[] args) {

		// declare and initialize scanner and prompt user for a value as the size of array and store in an integer variable
		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter size of array: ");
		int arraySize = refScanner.nextInt();

		// declare and initialize an array with array size of user input value
		int[] userDefineArray = new int[arraySize];

		// prompt user and store values into user defined array 
		for (int i = 0; i < userDefineArray.length; i++) {
			System.out.print("Enter a number: ");
			int userInput = refScanner.nextInt();
			userDefineArray[i] = userInput;
		}

		// get the array length
		int arrayLength = arraySize;
		
		// Sort Array with sortArray method ()
		RemoveDuplicate.sortArray(userDefineArray);

		// Remove Duplicates
		arrayLength = RemoveDuplicate.removeDuplicateElements(userDefineArray, arraySize);

		// Print new Array
		for (int i = 0; i < arrayLength; i++) {
			System.out.print(
					userDefineArray[i] != userDefineArray[arrayLength - 1] ? userDefineArray[i] + ", " : userDefineArray[i]);
		}
		refScanner.close();
	} // end of main()

}// end of class Problem13

//	Write a Java program to remove the duplicate elements of a given array and return the new length of the array.
//	Sample array: [20, 20, 30, 40, 50, 50, 50]
//	After removing the duplicate elements the program should return 4 as the new length of the array.

//	Output:
//	Enter size of array: 7
//	Enter a number: 20
//	Enter a number: 20
//	Enter a number: 30
//	Enter a number: 40
//	Enter a number: 50
//	Enter a number: 50
//	Enter a number: 50
//	20, 30, 40, 50