package com.optimum.problem13;

public class RemoveDuplicate {

	public static void sortArray(int [] array) {
		
		int temp;
		
		// nested for loop checks if the current value at i is more than
		// the next current value of the array, store the value in a temp
		// assign the smaller value of the next index to i
		// assign the larger value stored at temp to the next current index
		for (int i = 0; i < array.length; i++) {
			for (int j = i+1; j < array.length; j++) {
				if (array[i] > array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
	}// end of static sortArray() method
	
	// method takes in the array and array length value
	// for loop runs till the end of the array
	// condition checked if the current index value is not same as the next index value
	// store the current index value 
	public static int removeDuplicateElements(int [] array, int arrayLength) {
		
		int j = 0;
		
		for (int i = 0; i < arrayLength - 1; i++) {
			if(array[i] != array[i+1]) {
				array[j] = array[i];
				j++;
			}
		}

		// store the last value of the array in the new array 
		array[j] = array[arrayLength-1];
		j++;
		return j;
	}// end of static removeDuplicateElements() method
}
