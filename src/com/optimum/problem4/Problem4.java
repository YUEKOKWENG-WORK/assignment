package com.optimum.problem4;

import java.util.Scanner;

public class Problem4 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");		
		int userInput = refScanner.nextInt();		
		refScanner.close();

		for (int i = userInput; i > 0; i--) { // first for loop starts from the value of user input and decrement till more than 0
			for (int j = 0; j < i; j++) { // second for loop will check the value of j must be less than the current value of i as i decreases
				System.out.print("*");
			} // end of j
			System.out.println();				
		} // end of i
	}// end of main()
} // end of Problem4 class

//	Output:
//	Enter a number: 4
//	****
//	***
//	**
//	*