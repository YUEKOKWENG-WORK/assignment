package com.optimum.problem11;

public class RemoveDuplicates {
	
	public static int removeDuplicateElements(int [] array, int length) {
		// if there is no value in the array or a single element
		// print out message and return the array length
		if (length == 0 || length == 1) {
			System.out.println("Nothing to Sort");
			return length;
		}
		
		int j = 0;
		
		// for loop runs till end of array length
		for (int i = 0; i < length - 1; i++) {
			// condition to check if the current array index value is not the same 
			// as the next array index value
			if (array[i] != array[i+1]) {
				// assign the value of current array index to the first array value
				array[j] = array[i];
				j++; // increment j - increment to the next array index
			}
		}
		// assign the last value of the array index to the last value of the array
		array[j] = array[length-1];
		// increment j as the final number of elements in the array
		j++;
		return j;
	} // end of removeDuplicateElements method
} // end of RemoveDuplicates Class
