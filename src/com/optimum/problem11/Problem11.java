package com.optimum.problem11;

import java.util.Arrays;

public class Problem11 {

	public static void main(String[] args) {

		// declare and initialize an array with values
		int [] array = {1, 2, 3, 1, 2, 3, 4};
		// sort the array in ascending order {1, 1, 2, 2, 3, 3, 4}
		Arrays.sort(array);
		// store the integer value of the array length
		int length = array.length;
		// call the method of removeDuplicateElements that takes in the array and length of array as arguments
		// a new length of the array will be returned with the array of only unique elements
		length = RemoveDuplicates.removeDuplicateElements(array, length);
		for (int i = 0; i < length; i++) {
			System.out.print(array[i] != array[length-1] ? array[i] + ", " : array[i]);
		}
	} // end of main()
} // end of Problem11 Class

//	Remove duplicate elements from an array
//	Example:
//	Input array elements: 
//	1, 2, 3, 1, 2, 3, 4
//	Output: 
//	Elements after removing duplicates 
//	1, 2, 3, 4