package com.optimum.problem1;

import java.util.Scanner;

public class Problem1 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in); 
		System.out.print("Enter a Number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();
		for (int i = 0; i < userInput; i++) { // first for loop will run till user input minus 1
			for (int j = 0; j <= i; j++) { // second for loop will check the value of first for loop and check condition till equal value of first for loop
				System.out.print("*"); // print() method is used
			} // end of j
			System.out.println(); // once second for loop is done, a new line is printed
		} // end of i		
	} // end of main()
} // end of Problem1 class

//	Output:
//	Enter a Number: 4
//	*
//	**
//	***
//	****