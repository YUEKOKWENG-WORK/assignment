package com.optimum.problem9;

import java.util.Arrays;
import java.util.Scanner;

public class Problem9 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter the size of an Array: "); // prompt user for a number
		int userArrayInput = refScanner.nextInt(); // store the number as the size of array to a integer variable
		int [] newArray  = new int [userArrayInput]; // declare and initialize an array of size declared by the user
		
		for (int i = 0; i < newArray.length; i++) { // run a for loop till the number of array length in line 13
			System.out.print("Enter a number: "); // prompt use to enter a number
			int userInput = refScanner.nextInt(); // store user input into a integer variable
			newArray[i] = userInput;	// assign the value of user input into the current index of the array
		} // end of for loop
		
		String displayArray = userArrayInput == 1 ? "Number in Array: " : "Numbers in Array: ";
		System.out.print(displayArray); // output number of elements in the array to user
		
		for (int i : newArray) {
			System.out.print(i + " "); // iterate through the array and display the value to the user
		}
		
		System.out.println();
		System.out.print("Enter a number to check inside of Array: "); // prompt user to enter a value to check if inside the array
		int userValue = refScanner.nextInt(); // store user value in a integer variable
		
		boolean contains = Arrays.stream(newArray).anyMatch((x) -> x == userValue); // the stream is a method in the Array class that is used to get a sequential stream from the array
		// passed as a parameter with its elements. 
		// anyMatch method returns a boolean if any elements in the stream matches the user value
		String result = contains == true ? userValue + " is in the array" : userValue + " is not in the array" ;
		System.out.println(result); // printing out if the value is present or not in the array
		
		refScanner.close();
	} // end of main()

} // end of class problem9

//	Output 1:
//	Enter the size of an Array: 5
//	Enter a number: 44
//	Enter a number: 57
//	Enter a number: 18
//	Enter a number: 31
//	Enter a number: 29
//	Numbers in Array: 44 57 18 31 29 
//	Enter a number to check inside of Array: 18
//	18 is in the array

//	Output 2:
//	Enter the size of an Array: 5
//	Enter a number: 15
//	Enter a number: 35
//	Enter a number: 75
//	Enter a number: 95
//	Enter a number: 45
//	Numbers in Array: 15 35 75 95 45 
//	Enter a number to check inside of Array: 22
//	22 is not in the array

