package com.optimum.problem5;

import java.util.Scanner;

public class Problem5 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();
		
		for (int i = userInput; i > 0; i--) { // starts from user input value and decrement till more than 0
			for (int j = 1; j <= i; j++) { // set the value of j to 1 and increase till equals to i
				System.out.print(j);
			} // end of j
			System.out.println();
		} // end of i
	}// end of main()
}// end of Problem5 class

//	Output:
//	Enter a number: 5
//	12345
//	1234
//	123
//	12
//	1