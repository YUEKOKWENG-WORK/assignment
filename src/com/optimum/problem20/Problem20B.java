package com.optimum.problem20;

//	Java Program to check if a String is a palindrome or not without reverse of a String.
//	Example : madam is a palindrome
//	program is not a palindrome

public class Problem20B {

	public static void main(String[] args) {
		
		// declare and initialize a String variable with a value
		String userInput = "mada2";
		// declare and initialize a copy of the String variable with a value
		String temp = userInput;
		// declare and initialize a String variable empty to get the reverse value
		String reverseString = "";
		// declare and initialize a integer variable to get the length of the string
		int length = temp.length();
		
		// declare and initialize a for loop at starts from the end of the string of (length-1) and iterate through each character into the reverseString variable
		for (int i = length-1 ; i >= 0; i--) {
			reverseString += temp.charAt(i);		
		}
		
		// check the reverseString ignoring case to the copy of the initial string value
		// display to user if is palidrone or not
		System.out.println(reverseString.equalsIgnoreCase(temp) ? temp + " is a palindrome" : temp + " is a not palindrome");
	} // end of main()

} // end of Problem20B class
