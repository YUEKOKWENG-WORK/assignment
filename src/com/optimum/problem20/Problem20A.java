package com.optimum.problem20;

//	Java Program to check if a String is a palindrome or not using reverse of a String.
//	Example : madam is a palindrome
//	program is not a palindrome

public class Problem20A {

	public static void main(String[] args) {
		
		// declare and initialize a String variable with a value
		String userInput = "madam";
		// creating a new instance of a StringBuilder
		StringBuilder refStringBuilder = new StringBuilder(userInput);
		// using the reverse method and toString method and ignoring case to check the reverse of the string is same as the string in line 12.
		// display to user if is palidrone or not
		System.out.println(refStringBuilder.reverse().toString().equalsIgnoreCase(userInput) ? userInput + " is a palidrome" : userInput + " is not a palidrome");	
	} // end of main()
} // end of Problem20A class
