package com.optimum.problem7;

import java.util.Scanner;

public class Problem7 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();
		int i,j,m;
		int k = 1; 
		
		// Increment of each row values is by 2 i.e (1,3,5,7,9)
		for ( i = 1; i <= userInput; i++) { // for loop runs till less than and equal to user input and start at 1
			for ( j = i; j <= k; j++) {	// for loop prints till less than or equal to k (initial value is 1)
				
				System.out.print(j); // in the second iteration, k = 3 and j = i (i.e prints 2 and 3)
			}
			for ( m = k; m > i; m--) {	 // in the second iteration, k = 3 and i = 2 (i.e prints 2 (3-1))
				System.out.print(m-1);
			}
			k+=2; // increment the value of k by 2
			System.out.println();
		}
		
	} // end of main()
} // end of Problem7 class

//	Output:
//	1 
//	2 3 2 
//	3 4 5 4 3 
//	4 5 6 7 6 5 4
//	5 6 7 8 9 8 7 6 5