package com.optimum.problem12;

//import java.util.Arrays;

public class Problem12 {

	public static void main(String[] args) {

		// declare and initialize an array 
		int [] array = {55, 10, 8, 90, 43, 87, 95, 25, 50, 12};
		// assign an integer variable the length of the array
		int arrayLength = array.length;
		
		// Method 1
//		Arrays.sort(array);
//		System.out.println("Second largest: " + array[arrayLength-2]);
//		System.out.println("Second smallest: " + array[1]);
		
		// Method 2
		int temp; // initialize a temporary variable to temporary store a value
		for (int i = 0; i < array.length; i++) { // first for loop runs till end of array length
			for (int j = i+1; j < array.length; j++) { // second array starts from +1 of the current value of i
				if (array[i] > array[j]) { // if the value at index i is more than the value at index j, execute
					temp = array[i]; // assign the value at index i into the temp variable
					array[i] = array[j]; // assign the value at index j to index i
					array[j] = temp; // assign the temp value to index j
				} // end of if statement
			} // end of j for loop
		} // end of i for loop
		
		System.out.println("Second largest: " + array[arrayLength-2]); // to print second largest is array length -2
		System.out.println("Second smallest: " + array[1]);		// to print second smallest value is at index 1
	}// end of main()
} // end of Problem12 Class

//	Output:
//	Second largest: 90
//	Second smallest: 10