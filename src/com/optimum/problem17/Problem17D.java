package com.optimum.problem17;

//Find Factorial of a given n number using recursion
//factorial of n (n!) = 1 * 2 * 3 * 4 * ... * n
//Factorial of 6 = 720
//6 * 5 * 4 * 3 * 2 * 1 * 1 = 72

public class Problem17D {

	public static void main(String[] args) {
		int givenNumber = 6;
		System.out.println(givenNumber + "! = " + factorial(givenNumber));
	}
	
	public static int factorial(int n) {
		if(n == 1) {
			return 1;			
		}else {
			// call the same method with each time value of n -1 passed in till n ==1 
			return n * factorial(n - 1);
		}
	}// end of main() 
}// end of Problem17D Class

//	Output:
//	6! = 720