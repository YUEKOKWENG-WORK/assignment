package com.optimum.problem17;

//Find Factorial of a given n number using While Loop
//factorial of n (n!) = 1 * 2 * 3 * 4 * ... * n
//Factorial of 6 = 720
//6 * 5 * 4 * 3 * 2 * 1 * 1 = 72

public class Problem17C {

	public static void main(String[] args) {

		// declare and initialize factorial value, sum of factorial and a counter for the while loop condition
		int factorial = 6, sumOfFactorial = 1, counter = 1;
		
		// while condition for the value of the counter to be less than the factorial value
		while(counter <= factorial) {
			// sumOfFactorial = sumOfFactorial * counter
			sumOfFactorial *= counter;
			counter++;
		}
		System.out.println(factorial + "! = " + sumOfFactorial);
	} // end of main() 
} // end of Problem17C Class
//	Output:
//	6! = 720