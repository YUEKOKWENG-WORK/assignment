package com.optimum.problem17;
import java.math.BigInteger;

//Find Factorial of a given n number using BigInteger
//factorial of n (n!) = 1 * 2 * 3 * 4 * ... * n
//Factorial of 6 = 720
//6 * 5 * 4 * 3 * 2 * 1 * 1 = 72

public class Problem17B {

	public static void main(String[] args) {
		
		// declare and initialize a large integer value and big integer variable 
		int number = 50;
		BigInteger factorial = BigInteger.ONE; // same as new BigInteger("1");
		
		// condition to check if number is not less than 1
		// using BigInteger class method of multiply to assign the value of 1 times the BigInteger factorial
		if(number < 1) {
			System.out.println("Number cannot be less than 1");
		}else {
			for (int i = number; i >= 2; i--) {
				factorial = factorial.multiply(BigInteger.valueOf(i));			
			}
			System.out.println(number + "! = " + factorial);
		}
	} // end of main()
} // end of Problem17B class

//	Output:
//	50! = 30414093201713378043612608166064768844377641568960512000000000000
