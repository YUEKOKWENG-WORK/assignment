package com.optimum.problem17;

//	Find Factorial of a given n number using for loop
//	factorial of n (n!) = 1 * 2 * 3 * 4 * ... * n
//	Factorial of 6 = 720
//	6 * 5 * 4 * 3 * 2 * 1 * 1 = 720

public class Problem17A {

	public static void main(String[] args) {
		
		// declare and store the factorial number up to from a give number
		// declare and store the sum of the factorial number
		int factorialNumber = 6, sumOfFactorialNumber = 1;

		for (int i = 1; i <= factorialNumber; i++) {
			sumOfFactorialNumber *= i; // assign and store the value of sumOfFactorialNumber times i
		}		
		System.out.println(factorialNumber + "! = " + sumOfFactorialNumber);
	} // end of main()

} // end of Problem17A

//	Output:
//	6! = 720