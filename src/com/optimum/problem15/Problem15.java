package com.optimum.problem15;

import java.util.Scanner;

public class Problem15 {

	public static void main(String[] args) {

		// Declare an Array size
		int arraySize = 100;
		// Declare an Array of Product class
		Product [] refProduct = new Product [arraySize];
		
		Scanner refScanner = new Scanner(System.in);
		// Prompt user for inputs
		int userProductID, userProductQuantity;
		String userProductName;
		char userResponse;
		double userProductPrice;
		
		// Declaration for Price List
		double totalPrice = 0.0; 
		int discountNumber = 20;
		
		// for loop to prompt user input and store into the array
		for (int i = 0; i < arraySize; i++) {
			System.out.print("Enter Product ID: ");
			userProductID = refScanner.nextInt();
			refScanner.nextLine();
			System.out.println();
			
			System.out.print("Enter Product Name: ");
			userProductName = refScanner.nextLine();
			System.out.println();
			
			System.out.print("Enter Product Price: ");
			userProductPrice = refScanner.nextDouble();
			System.out.println();
			
			System.out.print("Enter Product Quantity: ");
			userProductQuantity = refScanner.nextInt();
			totalPrice += userProductPrice * userProductQuantity;
			System.out.println();
			
			// Store User input to Array
			refProduct[i] = new Product(userProductID,userProductName, userProductPrice, userProductQuantity);
			
			// condition check if user defined array is more than 1 element
			if(arraySize != 1) {
				System.out.print("Wish to Continue:(Y/N): ");
				userResponse = refScanner.next().charAt(0);
				userResponse = Character.toLowerCase(userResponse);
				System.out.println();
				
				// condition check to break of loop if user response 'n'
				if (userResponse == 'n') {
					break;
				}
			}
		}
		
		refScanner.close();
		// Print out product details
		System.out.println("Your product list as follows:");
		System.out.println();
		
		String headers = String.format("|%-10s| |%-25s| |%-10s| |%-10s|", "Product ID", "ProductName", "Price", "Quantity");
		System.out.println(headers);
		String divider = String.format("|%-55s|", "================================================================");
		System.out.println(divider);
		
		for (Product product : refProduct) {
			if(product == null) {
				break;
			}
			System.out.println(product);
		}
		double discountPercentage = discountNumber/100.0;
		double discountAmount = (totalPrice*discountPercentage);
		double amountPayable  = (totalPrice - discountAmount);
		// Print out price list
		System.out.println();
		System.out.println("Total Price\t" + totalPrice);
		System.out.println("Flat Discount\t" + discountNumber + "%");
		System.out.println("Discount Amount\t" + discountAmount);
		System.out.println("Amount to Pay\t" + amountPayable);
	
	} // end of main()
} // end of Problem15 Class

//	Output:
//	Enter Product ID: 101
//
//	Enter Product Name: Samsung Mobile
//
//	Enter Product Price: 700
//
//	Enter Product Quantity: 2
//
//	Wish to Continue:(Y/N): y
//
//	Enter Product ID: 201
//
//	Enter Product Name: Sony TV
//
//	Enter Product Price: 900
//
//	Enter Product Quantity: 4
//
//	Wish to Continue:(Y/N): Y
//
//	Enter Product ID: 301
//
//	Enter Product Name: Apple Watch
//
//	Enter Product Price: 500
//
//	Enter Product Quantity: 1
//
//	Wish to Continue:(Y/N): N
//
//	Your product list as follows:
//
//	|Product ID| |ProductName              | |Price     | |Quantity  |
//	|================================================================|
//	|101       | |Samsung Mobile           | |700.000000| |2         |
//	|201       | |Sony TV                  | |900.000000| |4         |
//	|301       | |Apple Watch              | |500.000000| |1         |
//
//	Total Price	5500.0
//	Flat Discount	20%
//	Discount Amount	1100.0
//	Amount to Pay	4400.0