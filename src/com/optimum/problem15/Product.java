package com.optimum.problem15;

public class Product {

	// declare variables of Product class
	int productID, quantity;
	String productName;
	double price;
	
	// Product constructor with 4 arguments
	public Product(int productID, String productName, double price, int quantity) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	} // end of product constructor

	// override toString method to return the value of the current product detail
	@Override
	public String toString() {
		String result = String.format("|%-10d| |%-25s| |%-10f| |%-10d|", productID, productName, price,quantity);
		return result;
	} // end of overridden toString()
}  // end of product class
