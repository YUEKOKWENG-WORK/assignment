package com.optimum.problem2;

import java.util.Scanner;

public class Problem2 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();
		
		for (int i = 0; i < userInput; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(j+1); // print the value of j + 1 as counter start at zero
			} // end of j
			System.out.println();
		} // end of i
	} // end of main()
} // end of Problem2 class

//	Output:
//	Enter a number: 5
//	1
//	12
//	123
//	1234
//	12345
