package com.optimum.problem14;

public class User {
	// declaration of private variables in User Class
	private String emailAddress;
	private String password;
	private String securityKey;
	private double bankBalance;

	// User constructor with 4 inputs as arguments
	public User(String emailAddress, String password, String securityKey, double bankBalance) {
		this.emailAddress = emailAddress;
		this.password = password;
		this.securityKey = securityKey;
		this.bankBalance = bankBalance;
	}

	// User constructor with no arguments
	public User() {
	}

	// setters and getter of private variables
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityKey() {
		return securityKey;
	}

	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}

	public double getBankBalance() {
		return bankBalance;
	}

	public void setBankBalance(double bankBalance) {
		this.bankBalance = bankBalance;
	}
} // end of class User
