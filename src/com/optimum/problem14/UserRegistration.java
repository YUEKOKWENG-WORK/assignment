package com.optimum.problem14;

import java.util.ArrayList;
import java.util.Scanner;

public class UserRegistration {

	static User refRegisterUser;

	public static ArrayList<User> refUserArray = new ArrayList<User>();

	static Scanner refScanner = new Scanner(System.in);
	static String setEmail = "";
	static String setPassword = "";
	static String setSecurityKey = "";
	static String retypedPassword = "";
	static double setbankBalance = 0.0;
	static int index = 0;

	public static void promptUserResetDetails() {
		System.out.print("Enter Your ID: ");
		setEmail = refScanner.next();
		refScanner.nextLine();
		System.out.print("Enter security key: ");
		setSecurityKey = refScanner.next();
	} // end of promptUserResetDetails()

	public static void promptSecurityKey() {
		System.out.print("What is favourite colour? ");
		setSecurityKey = refScanner.next();
		refScanner.nextLine();
		System.out.println(setSecurityKey + " is your security key, if you forget your password.");
	} // end of promptSecurityKey()

	public static void promptRetypePassword() {
		System.out.print("Re-type password: ");
		retypedPassword = refScanner.next();
		refScanner.nextLine();
	} // end of promptRetypePassword()

	public static void promptPassword() {
		System.out.print("Enter password: ");
		setPassword = refScanner.next();
		refScanner.nextLine();
	} // end of promptPassword()

	public static void promptNewPassword() {
		System.out.print("Enter new password: ");
		setPassword = refScanner.next();
		refScanner.nextLine();
	} // end of promptNewPassword()

	public static void promptEmail() {
		System.out.print("Enter email address: ");
		setEmail = refScanner.next();
	} // end of promptEmail()

	public static void initUser() {
		refRegisterUser = new User("xyz@gmail.com", "password", "green", 10.0);
		refUserArray.add(index, refRegisterUser);
		index++;
	} // end of initUser()

	public static void addNewUser() {
		refRegisterUser = new User(setEmail, setPassword, setSecurityKey, setbankBalance);
		refUserArray.add(index, refRegisterUser);
		index++;
	} // end of addNewUser()

	public static void registerUser() {
		// System.out.println("Registering User..");
		// initialize first user in line 59
		if (index == 0) {
			initUser();
		}
		promptEmail();
		// loop through the array list to check if email address is the same and re-enter till is unique
		for (User userRegistration : refUserArray) {
			while (setEmail.equals(userRegistration.getEmailAddress())) {
				System.out.println("email already exists!!");
				System.out.println();
				promptEmail();
			}
		}
		System.out.println();
		promptPassword();
		promptRetypePassword();

		// condition check of password re-entered is the same
		while (!retypedPassword.equals(setPassword)) {
			System.out.println();
			System.out.println("Password doesn't match!!");
			promptRetypePassword();
		}
		System.out.println();
		// prompt user security key
		promptSecurityKey();
		System.out.println();
		// Add new user once validated into array list
		addNewUser();
		System.out.println("Registration Successful!!");
		System.out.println();

// To print out array list of users		
//		for (User refUser : refUserArray) {
//			System.out.println(refUser.getEmailAddress() + " " + refUser.getPassword() + " " + refUser.getSecurityKey()
//					+ " " + refUser.getBankBalance());
//		}
	} // end of registerUser()

} // end of UserRegistration class
