package com.optimum.problem14;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PromptUser {

	public static int userChoice;

	public static void runProgram() {
		// call userInput()
		userInput();
	} // end of runProgram()

	// userInput() method prompts user input from 1 - 4
	// base on the value of user input will be directed to different methods 
	public static void userInput() {

		Scanner refScanner = new Scanner(System.in);
		System.out.println("User Home Page:");
		System.out.println("1. Register");
		System.out.println("2. Login");
		System.out.println("3. Forget Password");
		System.out.println("4. Logout(exit)");
		System.out.println();
		System.out.print("Enter Your Choice: ");
		try {
			userChoice = refScanner.nextInt();
			switch (userChoice) {
			case 1:
				System.out.println();
				// System.out.println("Register");
				UserRegistration.registerUser();
				userInput();
				break;
			case 2:
				System.out.println();
				// System.out.println("Login");
				UserLogin.userLogin();
				break;
			case 3:
				System.out.println();
				// System.out.println("Forget Password");
				ResetPassword.resetUserPassword();
				userInput();
				break;
			case 4:
				refScanner.close();
				System.out.println();
				System.out.println("Logout Successfully!!!");
				System.exit(0);
				break;

			default:
				System.out.println();
				System.out.println("..Invalid Input..");
				System.out.println();
				userInput();
				break;
			}
		} catch (InputMismatchException e) {
			System.out.println();
			System.out.println("..Invalid Input..");
			System.out.println();
			userInput();
		}
	} // end of userInput() method
} // end of PromptUser Class
