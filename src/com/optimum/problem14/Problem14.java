package com.optimum.problem14;

public class Problem14 {

	public static void main(String[] args) {
		PromptUser.runProgram();
	} // end of main()
} // end of class Problem14

// Program Output
//	User Home Page:
//	1. Register
//	2. Login
//	3. Forget Password
//	4. Logout(exit)
//	
//	Enter Your Choice: 1
//	
//	Enter email address: xyz@gmail.com
//	email already exists!!
//	
//	Enter email address: opt@gmail.com
//	
//	Enter password: xyz123
//	Re-type password: xyyz1234
//	
//	Password doesn't match!!
//	Re-type password: xyz123
//	
//	What is favourite colour? black
//	black is your security key, if you forget your password.
//	
//	Registration Successful!!
//	
//	User Home Page:
//	1. Register
//	2. Login
//	3. Forget Password
//	4. Logout(exit)
//	
//	Enter Your Choice: 2
//	
//	Enter email address: opt@gmail.com
//	Enter password: xyz123
//	
//	Login Successful!!
//	
//	Type 1 : Check Available Bank Balance
//	Type 2 : Deposit Amount
//	Type 3 : Withdraw Amount
//	
//	Enter Your Choice: 1
//	
//	Available Balance : 0.0 dollar
//	
//	Wish to Continue? (y/n) : y
//	
//	Type 1 : Check Available Bank Balance
//	Type 2 : Deposit Amount
//	Type 3 : Withdraw Amount
//	
//	Enter Your Choice: 2
//	
//	Enter Amount: -50
//	Amount can't be negative!!
//	
//	Enter Amount: 50
//	
//	50.0 dollar deposited successfully!!
//	
//	Wish to Continue? (y/n) : y
//	
//	Type 1 : Check Available Bank Balance
//	Type 2 : Deposit Amount
//	Type 3 : Withdraw Amount
//	
//	Enter Your Choice: 3
//	
//	Enter Amount: 100
//	
//	Sorry!! insufficient balance.
//	
//	Wish to Continue? (y/n) : y
//	
//	Type 1 : Check Available Bank Balance
//	Type 2 : Deposit Amount
//	Type 3 : Withdraw Amount
//	
//	Enter Your Choice: 30
//	
//	Choice not available!!
//	
//	Type 1 : Check Available Bank Balance
//	Type 2 : Deposit Amount
//	Type 3 : Withdraw Amount
//	
//	Enter Your Choice: 3
//	
//	Enter Amount: 30
//	
//	Transaction Successful!!
//	
//	Wish to Continue? (y/n) : y
//	
//	Type 1 : Check Available Bank Balance
//	Type 2 : Deposit Amount
//	Type 3 : Withdraw Amount
//	
//	Enter Your Choice: 1
//	
//	Available Balance : 20.0 dollar
//	
//	Wish to Continue? (y/n) : n
//	
//	Thanks for Banking with Us !!!
//	
//	User Home Page:
//	1. Register
//	2. Login
//	3. Forget Password
//	4. Logout(exit)
//	
//	Enter Your Choice: 3
//	
//	Enter Your ID: opt@gmail.com
//	Enter security key: black
//	
//	Enter new password: abc123
//	
//	Re-type password: xyz123
//	
//	Password doesn't match
//	
//	Re-type password: abc123
//	
//	What is favourite colour? red
//	red is your security key, if you forget your password.
//	
//	Your password has been reset successfully.
//	
//	User Home Page:
//	1. Register
//	2. Login
//	3. Forget Password
//	4. Logout(exit)
//	
//	Enter Your Choice: 4
//	
//	Logout Successfully!!!