package com.optimum.problem14;

import java.util.InputMismatchException;

public class UserLogin extends UserRegistration {

	private static char userResponse;
	static double userCurrentBalance = 0.0;
	static double userAmount = 0.0;
	static User refUserLogin = new User();

	// method to prompt user for email and password to login
	// checks if user email and password exist in the array list
	// store a new object reference of the User class
	// get the current bank balance of the account when account is authenticated
	public static void userLogin() {
		if (index == 0) {
			initUser();
		}
		promptEmail();
		promptPassword();
		for (User refUser : refUserArray) {
			if (setEmail.equals(refUser.getEmailAddress()) && setPassword.equals(refUser.getPassword())) {
				refUserLogin = refUser;
				userCurrentBalance = refUser.getBankBalance();
				System.out.println();
				System.out.println("Login Successful!!");
				System.out.println();
				loginDashboard();
			}
		}
		System.out.println();
		System.out.println("Invalid account!!");
		setEmail = "";
		setPassword = "";
		System.out.println();
		userLogin();
	} // end of userLogin() method

	// method to display the user functions to check balance, deposit amount and withdraw amount once authenticated
	public static void loginDashboard() {
		int userChoice;
		System.out.println("Type 1 : Check Available Bank Balance");
		System.out.println("Type 2 : Deposit Amount");
		System.out.println("Type 3 : Withdraw Amount");
		System.out.println();
		System.out.print("Enter Your Choice: ");
		if (UserRegistration.index == 0) {
			UserRegistration.initUser();
		}

		try {
			userChoice = refScanner.nextInt();
			switch (userChoice) {
			case 1:
				System.out.println();
				// System.out.println("Type 1 : Check Available Bank Balance");
				checkBalance();
				break;
			case 2:
				System.out.println();
				// System.out.println("Type 2 : Deposit Amount");
				depositAmount();
				break;
			case 3:
				System.out.println();
				// System.out.println("Type 3 : Withdraw Amount");
				withdrawAmount();
				break;

			default:
				System.out.println();
				System.out.println("Choice not available!!");
				System.out.println();
				loginDashboard();
				break;
			}
		} catch (InputMismatchException e) {
			System.out.println();
			System.out.println("Choice not available!!");
			System.out.println();
			loginDashboard();
		}
	} // end of loginDashboard()

	// method to display to the user the current bank balance
	public static void checkBalance() {
		System.out.println("Available Balance : " + userCurrentBalance + " dollar");
		checkContinue();
	} // end of checkBalance()

	// method to prompt user on the amount to deposit
	// condition check if user amount is below 0 or is zero to re-enter amount
	// once validated update user entered amount into the current user bank balance
	public static void depositAmount() {
		System.out.print("Enter Amount: ");
		userAmount = refScanner.nextInt();
		if (userAmount < 0) {
			System.out.println("Amount can't be negative!!");
			System.out.println();
			depositAmount();
		}
		if (userAmount == 0) {
			System.out.println("Enter Amount again!!");
			System.out.println();
			depositAmount();
		}
		userCurrentBalance += userAmount;
		refUserLogin.setBankBalance(userCurrentBalance);
		System.out.println();
		System.out.println(userAmount + " dollar deposited successfully!!");

		checkContinue();
	} // end of depositAmount()

	// method to prompt user on the amount to withdraw
	// condition check if user amount is below 0 or is zero to re-enter amount
	// once validated update user entered amount into the current user bank balance
	public static void withdrawAmount() {
		System.out.print("Enter Amount: ");
		userAmount = refScanner.nextInt();
		if (userAmount < 0) {
			System.out.println("Amount can't be negative!!");
			System.out.println();
			withdrawAmount();
		}
		if (userAmount > userCurrentBalance) {
			System.out.println();
			System.out.println("Sorry!! insufficient balance.");
			checkContinue();
		}
		if (userAmount == 0) {
			System.out.println("Enter Amount again!!");
			System.out.println();
			withdrawAmount();
		}
		userCurrentBalance -= userAmount;
		refUserLogin.setBankBalance(userCurrentBalance);
		System.out.println();
		System.out.println("Transaction Successful!!");
		checkContinue();
	} // end of withdrawAmount()

	// method to check if user which to continue inside its user account to balance, deposit amount and withdraw amount
	public static void checkContinue() {
		userResponse = ' ';
		System.out.println();
		System.out.print("Wish to Continue? (y/n) : ");
		userResponse = refScanner.next().charAt(0);
		userResponse = Character.toLowerCase(userResponse);
		if (userResponse == 'n') {
			System.out.println();
			System.out.println("Thanks for Banking with Us !!!");
			System.out.println();
			PromptUser.userInput();
		} else if (userResponse == 'y') {
			System.out.println();
			loginDashboard();
		} else {
			System.out.println();
			System.out.println("..Invalid Input..");
			checkContinue();
		}
		System.out.println();
	} // end of checkContinue()

} // end of UserLogin class
