package com.optimum.problem14;

public class ResetPassword extends UserRegistration {

	public static void resetUserPassword() {

		// call method to prompt user for reset password details in UserRegisration class line 20
		promptUserResetDetails();
		// iterate through array list of user and check if user exists
		for (User refUser : refUserArray) {
			// condition if user email password and security exists in the array list
			if (setEmail.equals(refUser.getEmailAddress()) && setSecurityKey.equals(refUser.getSecurityKey())) {
				System.out.println();
				promptNewPassword();
				System.out.println();
				promptRetypePassword();
				// prompt user to re-type password till equals to new password entered
				while (!retypedPassword.equals(setPassword)) {
					System.out.println();
					System.out.println("Password doesn't match");
					System.out.println();
					promptRetypePassword();
				}
				System.out.println();
				// prompt user for security key method
				promptSecurityKey();
				// once authenticated set new user password and security key of the current user
				refUser.setPassword(setPassword);
				refUser.setSecurityKey(setSecurityKey);
				System.out.println();
				System.out.println("Your password has been reset successfully.");
				System.out.println();
				// call the method for user to enter another option in PromptUser class line 14
				PromptUser.userInput();
			}
		}
		// condition if user account does not exist
		// reset the Strings setEmail and setSecurityKey
		// and call the same method to resetUserPassword()
		System.out.println();
		System.out.println("Account doesn't exists!!");
		System.out.println();
		setEmail = "";
		setSecurityKey = "";
		resetUserPassword();
		System.out.println();

	} // end of resetUserPassword()
} // end of ResetPassword class
