package com.optimum.problem19;

import java.util.Scanner;

//	Java Program to check whether a number is Palindrome or not using while loop
//	11221 is not a palindrome
//	13531 is a palindrome

public class Problem19A {

	public static void main(String[] args) {
		
		// declare and initialize a scanner, integer value to store a number from the user
		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();
		
		// declare and initialize the user input, temp and reverse number
		int number = userInput;
		int temp = number, reverseNumber = 0;
		
		// if condition if user input is less than 10 to display invalid input
		// single numbers have no use for reverse
		if(number < 10) {
			System.out.println("Invalid Input");
		}else {
			// while condition when number is more than 0
			// modulus 10 of the number will get the remainder of the last digit number
			// multiplying the number by 10 moves the reverse number in a tenth place
			// number divided by 10 will give you the 
			while(number > 0) {
				reverseNumber = reverseNumber*10 + number%10;
				number = number / 10;
			}
		}
		System.out.println(reverseNumber == temp ? temp + " is a Palindrome number" : temp + " is a not Palindrome number");		
	} // end of main()
} // end of Problem19A class
