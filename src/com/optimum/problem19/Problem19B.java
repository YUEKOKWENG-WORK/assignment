package com.optimum.problem19;

import java.util.Scanner;

//Java Program to check whether a number is Palindrome or not using for loop
//11221 is not a palindrome
//13531 is a palindrome

public class Problem19B {

	public static void main(String[] args) {
		// declare and initialize a scanner, string value to store a number from the user
		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		String userInput = refScanner.nextLine();
		refScanner.close();
		
		//String userNumber = "13531";
		// parsing the string integer to get the length of the value
		int number = Integer.parseInt(userInput);
		int temp = number;
		
		// if condition if user input is less than 10 to display invalid input
		// single numbers have no use for reverse
		if (number < 10) {
			System.out.println("Invalid Input");
		}else {
			// for loop condition to run till the length of the string
			// modulus 10 of the number will get the remainder of the last digit number
			// multiplying the number by 10 moves the reverse number in a tenth place
			// number divided by 10 will give you the 
			int reserveNumber = 0;
			for (int i = 0; i < userInput.length(); i++) {
				reserveNumber = reserveNumber*10 + number%10;
				number = number / 10;
			}
			System.out.println(reserveNumber == temp ? temp + " is a Palidrome number" :  temp + " is a not Palidrome number" );
		}
	} // end of main()
} // end of Problem19B class
