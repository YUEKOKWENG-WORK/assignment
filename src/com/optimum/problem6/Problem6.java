package com.optimum.problem6;

import java.util.Scanner;

public class Problem6 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();
		
		for (int i = 0; i < userInput; i++) { // increase till 1 less than user input
			for (int j = 0; j <= i; j++) { // increase till less than or equal to i
				System.out.print("*");
				if(j>1) { // condition if j is more than 1, print additional '*'
					System.out.print("*");
				} // end of if condition
			} // end of j
			System.out.println("\n");
		} // end of i
	} // end of main()
} // end of Problem6 class

//	Output:
//	Enter a number: 5
//	*
//	**
//	****
//	******
//	********