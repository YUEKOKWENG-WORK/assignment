package com.optimum.problem18;

import java.util.Scanner;

//	Program to Check Armstrong Number
//	A positive integer is called an Armstrong number of order n if
//	abcd... = an + bn + cn + dn + ...
//	In case of an Armstrong number of 3 digits, the sum of cubes of each digits is equal to the number itself. For example:
//	153 = 1*1*1 + 5*5*5 + 3*3*3 // 153 is an Armstrong number.

public class Problem18 {

	public static void main(String[] args) {
		// Armstrong - if addition of cubes of digits of the number gives the original number
		
		// using scaner to prompt user for input
		// store the value of user input into a integer variable
		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int userInput = refScanner.nextInt();
		refScanner.close();

		// declare and initial a variable to store the value of user input
		int number = userInput;
		// declare and initial a variable to store the sum of remainder cubed and the remainder of the user input in each iteration
		int sum = 0, temp = userInput, remainder = 0;
		
		// condition to check user input is more than 0
		if(number < 0) {
			System.out.println("Number cannot be less than 0");
		}else {
			// while condition user input is more than 0
			// number % 3 gets the digit of the last number
			// sum is the addition of the current sum with the cube of the remainder
			// number / 10 gets the second digit of the number
			while(number > 0) {
				remainder = number % 10;
				sum = sum + (remainder * remainder * remainder);
				number = number / 10;				
			} // end of while loop
			
			// if condition that the sum of cube remainders are equal to the user input
			// print is Armstrong number
			if(sum == temp) {
				System.out.println(temp + " is an Armstrong number");
			}else {
				System.out.println(temp + " is not an Armstrong number");
			} // end of if else statement if sum of remainder cube is equal to user input
		} // end of if else if user input is more than 0
		
	} // end of main()
} // end of Problem18 class

//	Output 1:
//	Enter a number: 153
//	153 is an Armstrong number

//	Output 2:
//	Enter a number: 385
//	385 is not an Armstrong number