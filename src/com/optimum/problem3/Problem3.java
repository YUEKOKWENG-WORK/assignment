package com.optimum.problem3;

import java.util.Scanner;

public class Problem3 {

	public static void main(String[] args) {

		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		
		int userInput = refScanner.nextInt();
		refScanner.close();
		char alphabet = 'A'; // assign the alphabet A as the first character
		
		for (int i = 0; i < userInput; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(alphabet); // print out the value of character of alphabet
			} // end of j
			alphabet++; // Using the ASCII decimal value of a char to increment the alphabet
			System.out.println();
		} // end of i		
	}// end of main()
} // end of Problem3 class

//	Output:
//	Enter a number: 5
//	A
//	BB
//	CCC
//	DDDD
//	EEEEE